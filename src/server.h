#pragma once

#include "options.h"
#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include <cstddef>
#include <list>
#include <vector>
#include <queue>

class server {
public:
	server(const options& opt);
	void run();

private:
	class client {
	public:
		boost::shared_ptr< boost::asio::ip::tcp::socket > socket;
		std::vector<boost::uint8_t> buf;
		std::size_t size;					// Размер данных в буфере
		bool isRead;						// Флаг, означает, что сервер ожидает сообщение от клиента

		client(boost::asio::io_service& ios);
	};

	// Обрывает связь с клиентом и удаляет его из списка
	void disconnect(std::list<client>::iterator clientIt);

	// Инициализация последовательного порта и его настройка
	bool serialInit();

	// Удалить данные из буфера
	void serialFlush();

	// Заблокировать последовательный порт и начать запись, если порт уже заблокирован, запись будет начата при разблокировке
	void serialLockWrite(std::list<client>::iterator clientIt);

	// Разблокировать последовательный порт и предоставить доступ следующему в очереди
	void serialUnlock();

	// Событие - последовательный порт разблокирован, начинает запись в порт
	void serialUnlockHandler(const boost::system::error_code& error);

	// Событие - был получен сигнал завершения работы
	void signalHandler(const boost::system::error_code& error, int signal_number);

	// Событие - установлено соединение с клиентом
	void acceptHandler(const boost::system::error_code& error, std::list<client>::iterator clientIt);

	// Событие - принято сообщение клиента
	void socketReadHandler(const boost::system::error_code& error, std::size_t bytes_transferred, std::list<client>::iterator clientIt);

	// Событие - сообщение передано в последовательный порт
	void serialWriteHandler(const boost::system::error_code& error, std::size_t bytes_transferred, std::list<client>::iterator clientIt);

	// Событие - время задержки после записи в последовательный порт вышло
	void serialWriteDelayHandler(const boost::system::error_code& error, std::list<client>::iterator clientIt);

	// Событие - сообщение получено из последовательного порта
	void serialReadHandler(const boost::system::error_code& error, std::size_t bytes_transferred, std::list<client>::iterator clientIt);

	// Событие - таймаут чтения последовательного порта
	void serialReadTimeoutHandler(const boost::system::error_code& error);

	// Событие - сообщение передано клиенту
	void socketWriteHandler(const boost::system::error_code& error, std::size_t bytes_transferred, std::list<client>::iterator clientIt);

	unsigned short port;									// TCP порт для входящих сообщений
	unsigned timeout;										// Таймаут чтения последовательного порта
	unsigned delay;											// Максимальная задержка между частями одного сообщения последовательного порта
	std::string serialName;									// Адрес последовательного порта

	boost::asio::io_service ios;
	boost::asio::serial_port serial;
	boost::asio::signal_set signals;
	boost::asio::ip::tcp::acceptor acceptor;
	boost::asio::deadline_timer timeoutTimer;
	std::list<client> clientLst;							// Список подключенных клиентов плюс один клиент, ожидающий подключения
	std::queue< std::list<client>::iterator > serialQueue;	// Очередь клиентов ожидающих разблокировки последовательного порта
	boost::asio::deadline_timer serialMtx;					// Используется для организации блокировки последовательного порта
	bool isShutdown;										// Был получен сигнал завершения работы (SIGINT, SIGTERM)
};
