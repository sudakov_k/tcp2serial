/*
 * options.cpp
 *
 *  Created on: 08 июня 2015 г.
 *      Author: konstantin
 */

#include <boost/program_options.hpp>
#include <sstream>
#include <iostream>
#include "options.h"

using namespace boost::program_options;
using namespace boost;

options::options(int argc, const char* const argv[]) :
	help(false),
	port(4001),
	timeout(200),
	delay(20),
	serial("/dev/ttyS0"),
	isError(false)
{
	options_description desc("Options");
	desc.add_options()
		("help,h",									"print help and exit")
		("port,p",		value<unsigned short>(),	"TCP port (default: 4001)")
		("timeout,t",	value<unsigned>(),			"serial port read timeout, ms (default: 200)")
		("delay,d",		value<unsigned>(),			"serial port maximum pause in message, ms (default: 20)")
		("serial,s",	value<std::string>(),		"serial port (default: \"/dev/ttyS0\")")
		("config,c",	value<std::string>(),		"config file (a command line setting will be overwritten)");

	{
		std::stringstream ss;
		ss << desc;
		helpStr = ss.str();
	}

	std::string confFile;

	try {
		variables_map vm;
		store(parse_command_line(argc, argv, desc), vm);
		notify(vm);

		if (vm.count("help")) 		{ help 		= true; }
		if (vm.count("port"))		{ port		= vm["port"].as<unsigned short>(); }
		if (vm.count("timeout"))	{ timeout	= vm["timeout"].as<unsigned>(); }
		if (vm.count("delay"))		{ delay		= vm["delay"].as<unsigned>(); }
		if (vm.count("serial"))		{ serial	= vm["serial"].as<std::string>(); }
		if (vm.count("config"))		{ confFile	= vm["config"].as<std::string>(); }
	} catch(...) {
		std::cerr << "error: parse parameters" << std::endl;
		isError = true;
	}

	if (!isError && !confFile.empty()) {
		try {
			variables_map vm;
			store(parse_config_file<char>(confFile.c_str(), desc), vm);
			notify(vm);

			if (vm.count("port"))		{ port		= vm["port"].as<unsigned short>(); }
			if (vm.count("timeout"))	{ timeout	= vm["timeout"].as<unsigned>(); }
			if (vm.count("delay"))		{ delay		= vm["delay"].as<unsigned>(); }
			if (vm.count("serial"))		{ serial	= vm["serial"].as<std::string>(); }
		} catch(...) {
			std::cerr << "error: config file" << std::endl;
			isError = true;
		}
	}
}

std::string options::getHelp() {
	return helpStr;
}
