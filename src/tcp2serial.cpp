#include "options.h"
#include "server.h"
#include <syslog.h>
#include <iostream>

int main(int argc, char* argv[]) {
	options opt(argc, argv);
	if (opt.isError) {
		std::cout << opt.getHelp() << std::endl;
		return 1;
	}

	if (opt.help) {
		std::cout << opt.getHelp() << std::endl;
		return 0;
	}

	openlog("tcp2serial", LOG_PID | LOG_CONS, LOG_USER);

	syslog(LOG_INFO, "started");
	syslog(LOG_INFO, "TCP port: %u", opt.port);
	syslog(LOG_INFO, "serial port: %s", opt.serial.c_str());
	syslog(LOG_INFO, "serial port read timeout: %u", opt.timeout);
	syslog(LOG_INFO, "serial port maximum message pause: %u", opt.delay);

	server(opt).run();

	closelog();
	return 0;
}
