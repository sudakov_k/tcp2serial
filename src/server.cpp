#include "server.h"
#include <boost/bind.hpp>
#include <boost/system/error_code.hpp>

#include <syslog.h>
#include <signal.h>
#include <termios.h>
#include <exception>

using namespace boost;
using std::list;
using std::vector;
using std::string;
using std::size_t;

// public
server::server(const options& opt) :
	port(opt.port),
	timeout(opt.timeout),
	delay(opt.delay),
	serialName(opt.serial),
	ios(),
	serial(ios),
	signals(ios),
	acceptor(ios),
	timeoutTimer(ios),
	serialMtx(ios),
	isShutdown(false)
{
}

void server::run() {
	if (serialInit()) {
		try {
			// Выход по сигналам SIGINT и SIGTERM
			signals.add(SIGINT);
			signals.add(SIGTERM);
			signals.async_wait(bind(&server::signalHandler, this, asio::placeholders::error, asio::placeholders::signal_number));

			asio::ip::tcp::endpoint endpoint(asio::ip::tcp::v4(), port);
			acceptor.open(endpoint.protocol());
			acceptor.set_option(asio::ip::tcp::acceptor::reuse_address(true));
			acceptor.bind(endpoint);
			acceptor.listen(asio::ip::tcp::socket::max_connections);

			list<client>::iterator it = clientLst.insert(clientLst.end(), client(ios));
			acceptor.async_accept(*it->socket, bind(&server::acceptHandler, this, asio::placeholders::error, it));

			ios.run();
		} catch(const system::system_error& e) {
			syslog(LOG_INFO, "error run, %s", e.what());
		}

		serial.close();
	}
}

// private
server::client::client(asio::io_service& ios) : socket(new boost::asio::ip::tcp::socket(ios)), buf(vector<uint8_t>(1024)), size(0), isRead(false) {}

void server::disconnect(list<client>::iterator clientIt) {
	// Отключает клиента
	if (clientIt->socket->is_open()) {
		boost::system::error_code ec;

		{
			asio::ip::tcp::endpoint endpoint = clientIt->socket->remote_endpoint(ec);

			if (!ec) {
				syslog(LOG_INFO, "disconnect, %s:%u", endpoint.address().to_string().c_str(), endpoint.port());
			} else {
				syslog(LOG_INFO, "error disconnect, remote_endpoint(), %s", ec.message().c_str());
				syslog(LOG_INFO, "disconnect, unknown");
			}
		}

		clientIt->socket->shutdown(asio::ip::tcp::socket::shutdown_both, ec);
		if (ec) {
			syslog(LOG_INFO, "error disconnect, shutdown(), %s", ec.message().c_str());
		}

		clientIt->socket->close(ec);
		if (ec) {
			syslog(LOG_INFO, "error disconnect, close(), %s", ec.message().c_str());
		}
	}

	// Удаляет клиента из списка
	clientLst.erase(clientIt);
}

bool server::serialInit() {
	try {
		// Открытие последовательного порта, установка параметров обмена
		serial.open(serialName);
		serial.set_option(asio::serial_port::baud_rate(115200));
		serial.set_option(asio::serial_port::flow_control(asio::serial_port::flow_control::none));
		serial.set_option(asio::serial_port::parity(asio::serial_port::parity::none));
		serial.set_option(asio::serial_port::stop_bits(asio::serial_port::stop_bits::one));
		serial.set_option(asio::serial_port::character_size(8));
	} catch(const system::system_error& e) {
		syslog(LOG_INFO, "error serialInit, %s", e.what());

		if (serial.is_open()) { serial.close(); }
	}

	return serial.is_open();
}

void server::serialFlush() {
	tcflush(serial.native_handle(), TCIOFLUSH);
}

void server::serialLockWrite(list<client>::iterator clientIt) {
	// serialUnlockHandler сработает после вызова serialMtx.cancel_one()
	serialMtx.async_wait(bind(&server::serialUnlockHandler, this, asio::placeholders::error));

	// Если очередь пуста, запись можно начать немедленно
	if (serialQueue.empty()) {
		serialMtx.cancel_one();
	}

	// Клиент помещается в очередь
	serialQueue.push(clientIt);
}

void server::serialUnlock() {
	// Чтение последовательного порта завершено, удаляем клиента из очереди на последовательный порт
	serialQueue.pop();

	// Если очередь не пуста, разрешаем доступ к последовательному порту для следующего клиента
	if (!serialQueue.empty()) {
		serialMtx.cancel_one();
	}
}

void server::serialUnlockHandler(const system::error_code& error) {
	// error имеет значение operation_aborted т.к. handler вызван функцией cancel_one()
	if (error == asio::error::operation_aborted) {
		// Последовательный порт разблокирован
		list<client>::iterator clientIt = serialQueue.front();

		//std::cout << "Serial write start" << std::endl;
		// Перед записью необходимо очистить буфер последовательного порта, т.к. в нем может оказаться предыдущее сообщение
		serialFlush();
		asio::async_write(serial, asio::buffer(clientIt->buf, clientIt->size),
				bind(&server::serialWriteHandler, this, asio::placeholders::error, asio::placeholders::bytes_transferred, clientIt));
	} else {
		syslog(LOG_INFO, "error serialUnlockHandler, %s", error.message().c_str());
	}
}

void server::signalHandler(const system::error_code& error, int /*signal_number*/) {
	if (!error) {
		// Получен сигнал завершения работы
		syslog(LOG_INFO, "shutdown");

		isShutdown = true;
		// Завершение работы
		try {
			// Отмена получения сообщений
			for (list<client>::iterator it = clientLst.begin(), itEnd = clientLst.end(); it != itEnd; ++it) {
				if (it->isRead) { it->socket->cancel(); }
			}

			// Отмена новых соединений с клиентами
			acceptor.cancel();
		} catch(std::exception& e) {
			syslog(LOG_INFO, "error signalHandler, %s", e.what());
		}
	}
}

void server::acceptHandler(const system::error_code& error, list<client>::iterator clientIt) {
	if (!error) {
		// Ожидание следующего клиента
		list<client>::iterator it = clientLst.insert(clientLst.end(), client(ios));
		acceptor.async_accept(*it->socket, bind(&server::acceptHandler, this, asio::placeholders::error, it));

		{
			boost::system::error_code ec;
			asio::ip::tcp::endpoint endpoint = clientIt->socket->remote_endpoint(ec);

			if (!ec) {
				syslog(LOG_INFO, "connect, %s:%u", endpoint.address().to_string().c_str(), endpoint.port());
			} else {
				syslog(LOG_INFO, "error acceptHandler, remote_endpoint(), %s", ec.message().c_str());
				syslog(LOG_INFO, "connect, unknown");
			}
		}

		// Получение сообщения
		clientIt->isRead = true;
		clientIt->socket->async_read_some(asio::buffer(clientIt->buf),
				bind(&server::socketReadHandler, this, asio::placeholders::error, asio::placeholders::bytes_transferred, clientIt));
	} else {
		disconnect(clientIt);
	}
}

void server::socketReadHandler(const system::error_code& error, size_t bytes_transferred, list<client>::iterator clientIt) {
	clientIt->isRead = false;

	if (!error) {
		// Сообщение принято
		clientIt->size = bytes_transferred;
		serialLockWrite(clientIt);
	} else if (error == asio::error::eof || error == asio::error::operation_aborted) {
		// Клиент отключился от сервера или операция отменена (завершение работы)
		disconnect(clientIt);
	} else {
		// Неизвестная ошибка
		syslog(LOG_INFO, "error socketReadHandler, %s", error.message().c_str());

		disconnect(clientIt);
	}
}

void server::serialWriteHandler(const system::error_code& error, size_t /*bytes_transferred*/, list<client>::iterator clientIt) {
	if (!error) {
		//std::cout << "Serial write end" << std::endl;

		// Чтение последовательного порта
		//std::cout << "Serial read start" << std::endl;
		clientIt->size = 0;
		serial.async_read_some(
				asio::buffer(clientIt->buf.data() + clientIt->size, clientIt->buf.size() - clientIt->size),
				bind(&server::serialReadHandler, this, asio::placeholders::error, asio::placeholders::bytes_transferred, clientIt));

		// Таймер остановки по таймауту
		timeoutTimer.expires_from_now(posix_time::milliseconds(timeout));
		timeoutTimer.async_wait(bind(&server::serialReadTimeoutHandler, this, asio::placeholders::error));
	} else {
		syslog(LOG_INFO, "error serialWriteHandler, %s", error.message().c_str());

		disconnect(clientIt);
	}
}

void server::serialReadHandler(const system::error_code& error, size_t bytes_transferred, list<client>::iterator clientIt) {
	if (!error) {
		// Часть посылки принята
		//std::cout << "Serial read, size += " << bytes_transferred << std::endl;

		clientIt->size += bytes_transferred;

		// Чтение следующей части посылки
		// Проверка на переполнение буфера
		if (clientIt->size < clientIt->buf.size()) {
			serial.async_read_some(
					asio::buffer(clientIt->buf.data() + clientIt->size, clientIt->buf.size() - clientIt->size),
					bind(&server::serialReadHandler, this, asio::placeholders::error, asio::placeholders::bytes_transferred, clientIt));

			// Перезапуск таймера, ожидание следующей части посылки
			timeoutTimer.cancel();
			timeoutTimer.expires_from_now(posix_time::milliseconds(delay));
			timeoutTimer.async_wait(bind(&server::serialReadTimeoutHandler, this, asio::placeholders::error));
		} else {
			// Переполнение буфера
			syslog(LOG_INFO, "error serialReadHandler, %s", system::errc::make_error_code(system::errc::no_buffer_space).message().c_str());

			// Разблокировать последовательный порт, предоставить доступ следующему в очереди
			serialUnlock();

			disconnect(clientIt);
		}
	} else if (error == asio::error::operation_aborted) {
		// asio::error::operation_aborted возвращается после вызова функции cancel()
		// если clientIt->size == 0, то данные не получены, чтение завершено по таймауту
		//if (clientIt->size == 0) {
		//	// Таймаут
		//	std::cout << "Serial read timeout" << std::endl;
		//} else {
		//	// Конец посылки
		//	std::cout << "Serial read end, size = " << clientIt->size << std::endl;
		//}

		// Разблокировать последовательный порт, предоставить доступ следующему в очереди
		serialUnlock();

		// Передача сообщения клиенту
		asio::async_write(*clientIt->socket, asio::buffer(clientIt->buf, clientIt->size),
				bind(&server::socketWriteHandler, this, asio::placeholders::error, asio::placeholders::bytes_transferred, clientIt));
	} else {
		syslog(LOG_INFO, "error serialReadHandler, %s", error.message().c_str());
	}
}

void server::serialReadTimeoutHandler(const system::error_code& error) {
	if (!error) {
		// Чтение последовательного порта отменяется
		serial.cancel();
	}
}

void server::socketWriteHandler(const system::error_code& error, size_t /*bytes_transferred*/, list<client>::iterator clientIt) {
	if (!error) {
		// Запись завершена, чтение следующего сообщения
		if (!isShutdown) {
			clientIt->isRead = true;
			clientIt->socket->async_read_some(asio::buffer(clientIt->buf),
					bind(&server::socketReadHandler, this, asio::placeholders::error, asio::placeholders::bytes_transferred, clientIt));
		} else {
			// Завершение работы
			disconnect(clientIt);
		}
	} else {
		syslog(LOG_INFO, "error socketWriteHandler, %s", error.message().c_str());

		disconnect(clientIt);
	}
}
