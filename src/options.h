/*
 * options.h
 *
 *  Created on: 08 июня 2015 г.
 *      Author: konstantin
 */

#pragma once

#include <string>

class options {
public:
	mutable bool help;
	mutable unsigned short port;
	mutable unsigned timeout;
	mutable unsigned delay;
	mutable std::string serial;
	mutable bool isError;

	options(int argc, const char* const argv[]);
	std::string getHelp();

private:
	std::string helpStr;
};
