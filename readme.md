# Native build (Debian) #
## Dependency ##

* cmake
* build-essential
* libboost-system-dev
* libboost-program-options-dev

## Build ##
```
#!sh
mkdir tcp2serial_build
cd tcp2serial_build
cmake -D CMAKE_INSTALL_PREFIX=../tcp2serial_install ../tcp2serial
make
make install
```

# Crossbuild (Debian armhf) #
## Prepare ##

* [https://wiki.debian.org/CrossToolchains](https://wiki.debian.org/CrossToolchains)

## Dependency ##

* cmake
* crossbuild-essential-armhf
* libboost-system-dev:armhf
* libboost-program-options-dev:armhf

## Build ##
```
#!sh
mkdir tcp2serial_build
cd tcp2serial_build
CC=arm-linux-gnueabihf-gcc CXX=arm-linux-gnueabihf-g++ cmake -D CMAKE_INSTALL_PREFIX=../tcp2serial_install ../tcp2serial
make
make install
```

# Native build deb (Debian) #
## Dependency ##

* cmake
* dpkg-buildpackage
* build-essential
* libboost-system-dev
* libboost-program-options-dev

## Build ##
```
#!sh
dpkg-buildpackage
```

# Crossbuild deb (Debian armhf) #
## Prepare ##

* [https://wiki.debian.org/CrossToolchains](https://wiki.debian.org/CrossToolchains)

## Dependency ##

* cmake
* dpkg-buildpackage
* crossbuild-essential-armhf
* libboost-system-dev:armhf
* libboost-program-options-dev:armhf

## Build ##
```
#!sh
CC=arm-linux-gnueabihf-gcc CXX=arm-linux-gnueabihf-g++ dpkg-buildpackage -d
```